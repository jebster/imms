<?php

namespace Angus\Imms\php\classes;

class Helpers {

	/**
	 * @param $search
	 * @return void
	 */
	public static function searchAjax ($search): void {
		$ini = Bootstrapper::getIni();
		if ($search !== '') {
			$mdPath = $ini['app']['md_path'] ?? '/src/documents/';
			$searchDir = Bootstrapper::rootDirectory() . $mdPath;
			exec('grep -rli --exclude-dir=uploads ' . $searchDir . '* -e "' . $search . '"', $result);
			echo '<div>';
			if ($result === []) {
				echo '<a>No results.</a>';
			} else {
				self::loopThroughResults($result);
			}
			echo '<div>';
		}
	}

	/**
	 * @param string $textR
	 * @param string $r
	 * @return string
	 */
	private static function searchAjaxTextRLoop (string $textR, string $r): string {
		$rArray = explode('/', $r);
		for ($i = 0; $i < count($rArray); $i++) {
			$rArray[$i] = ucfirst($rArray[$i]);
			if ($i === (count($rArray) - 1)) {
				$textR .= $rArray[$i];
			} else {
				$textR .= $rArray[$i].' → ';
			}
		}

		return $textR;
	}

	/**
	 * @param array $result
	 * @return void
	 */
	private static function loopThroughResults (array $result): void {
		foreach ($result as $r) {
			$r = str_replace(Bootstrapper::rootDirectory() . '/src/documents/', '', $r);
			$r = str_replace('.md', '', $r);
			$textR = '';
			if (str_contains($r, '/')) {
				$textR = self::searchAjaxTextRLoop($textR, $r);
			} else {
				$textR = ucfirst($r);
			}
			echo "<a onclick='fill($r)' href='/$r'>$textR</a>";
		}
	}
}