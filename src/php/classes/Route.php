<?php

namespace Angus\Imms\php\classes;
use League\CommonMark\Exception\CommonMarkException;
use ScssPhp\ScssPhp\Exception\SassException;

require_once '../vendor/autoload.php';

class Route {
	/**
	 * @var string
	 */
	private string $path;
	private bool $mdExists;
	private bool $htmlExists;
	private bool $cacheExists;
	private array|false $ini;
	private static int $counter;
	private string $htmlExt = '.html';
	private string $mdExt = '.md';
	private string $rootDir;
	/**
	 * @var string
	 */
	private string $mdPath;
	private static string $mdPathStatic;
	/**
	 * @var string
	 */
	private string $htmlPath;
	/**
	 * @var int
	 */
	private int $cacheEOL;

	/**
	 * Handles routing in general.
	 * Will always load rendered markdown (HTML files; see Cache.php).
	 * If there is no available HTML file, it will check if the MD exists, and render it if it does.
	 * If not, it will throw a 404.
	 * @throws SassException
	 * @throws CommonMarkException
	 */
	public function __construct() {
		$this->ini = Bootstrapper::getIni();

		$this->mdPath = $this->ini['app']['md_path'] ?? '/src/documents/';
		self::$mdPathStatic = $this->mdPath;
		$this->htmlPath = $this->ini['app']['html_path'] ?? '/web/assets/cache/';
		$this->cacheEOL = $this->ini['app']['cache_eol'] ?? 900;

		// Set the root directory
		$this->rootDir = Bootstrapper::rootDirectory();

		// Fetch current path
		$this->path = ltrim(urldecode($_SERVER['REQUEST_URI']),'/');

		// If path has "?" remove it from the path, as it will be read wrong.
		if (str_contains($this->path, '?')) {
			$this->path = substr($this->path, 0, strpos($this->path, '?'));
		}

		$defaultView = $this->ini['app']['default_view'] ?? 'index';

		// If path shows up empty, use the default view
		if ($this->path === '') {
			$this->path = $defaultView;
		}

		// Check if MD and HTML exists.
		$this->mdExists = file_exists($this->rootDir . $this->mdPath . $this->path . $this->mdExt);
		$this->htmlExists = file_exists($this->rootDir . $this->mdPath . $this->path . $this->htmlExt);
		$this->cacheExists = file_exists($this->rootDir . $this->htmlPath . $this->path . $this->htmlExt);

		$this->displayPage();
	}

	/**
	 * Displays the page, and caches it, if it isn't cached already.
	 * @throws SassException
	 * @throws CommonMarkException
	 */
	private function displayPage (): void {
		$cache = new Cache();

		if ($this->mdExists) {
			// Get path to cached HTML file
			$cacheFile = $this->rootDir . $this->htmlPath . $this->path . $this->htmlExt;

			if (!$this->cacheExists) {
				$cache->md2html($this->rootDir . $this->mdPath . $this->path . $this->mdExt);
			}

			$htmlFileAge = time()-filectime($cacheFile) > $this->cacheEOL;

			$mdFile = $this->rootDir . $this->mdPath . $this->path . $this->mdExt;

			// Render it or replace it if it is older than the configured cache EOL, or if file has changed.
			if ($htmlFileAge || $this->fileChanged($mdFile)) {
				$cache->md2html($mdFile);
			}

			// Show the html file
			echo file_get_contents($cacheFile);
		} elseif ($this->htmlExists) {
			$htmlFile = $this->rootDir . $this->mdPath . $this->path . $this->htmlExt;
			$cacheFile = $this->rootDir . $this->htmlPath . $this->path . $this->htmlExt;

			if (!$this->cacheExists) {
				$cache->md2html($htmlFile);
			}

			$cacheFileAge = time()-filectime($cacheFile) > $this->cacheEOL;

			if ($cacheFileAge || $this->fileChanged($htmlFile)) {
				$cache->md2html($htmlFile);
			}

			echo file_get_contents($cacheFile);
		} else{
			// Set the response code to 404
			http_response_code(404);

			$errorHTMLPath = $this->ini['app']['error_html_path'] ?? '/web/assets/cache/errors/';
			$errorMDPath = $this->ini['app']['error_md_path'] ?? '/src/errors/';

			// Create cache if the HTML file exists (it should)
			if (!file_exists($this->rootDir . $errorHTMLPath . '404' . $this->htmlExt)) {
				$cache->md2html($this->rootDir . $errorMDPath . '404' . $this->mdExt, ['error_page' => true]);
			}

			// Show 404
			echo file_get_contents($this->rootDir . $errorHTMLPath . '404' . $this->htmlExt);
		}
	}

	public function fileChanged ($file): bool {
		$checksumDir = Bootstrapper::rootDirectory() . '/src/checksums/';
		$newChecksum = hash_file('sha256', $file);

		$extension = pathinfo($file);
		$extension = $extension['extension'];

		$checksumFile = $checksumDir . $this->path . '.' . $extension;

		$oldChecksum = file_get_contents($checksumFile);

		return $newChecksum !== $oldChecksum;
	}

	/**
	 * Creates a menu based on a directory.
	 *
	 * @param $dir
	 * @param bool $alternatingColours
	 * @param int $cnt
	 * @return string
	 */
	public static function createMenu ($dir, bool $alternatingColours = false, int $cnt = 0): string {

		$ini = Bootstrapper::getIni();
		$html = '';

		// Used for alternating colours; empty if odd, 'class="isEven"' if even;
		// This is to display alternating colours in the sidebar.
		$altColourClass = '';							// Define variable to avoid notices
		if ($alternatingColours) {						// If $alternating colours is set to `true`
			self::$counter = $cnt;						// Set private counter equal to $cnr
			if (self::$counter % 2) {					// If counter is modulus 2
				$altColourClass = 'class="isEven"';		// Add the class to the empty variable.
			}
		}

		/*
		 * Builds the sidebar.
		 * Regardless of it being a directory or a file,
		 * it will create a link, but the directory will have '#' as its path,
		 * and the files will have the path to the file in the form of pretty URL.
		 * If alternating colours are enable, see above code, they will be added as well.
		 * This will continue building the <li>'s one by one until there are none left.
		 * If there are many, this could probably take a while...
		 */
		if (is_dir($dir)) {
			$html .= self::loopThroughEntries($dir, $altColourClass, $alternatingColours);

			if (basename($dir) == 'uploads') {
				$mdPath = self::$mdPathStatic;
				$uploadPath = $ini['app']['upload_path'] ?? '/web/uploads';
				$fromPath = Bootstrapper::rootDirectory() . $mdPath . basename($dir).'/';
				$toPath = Bootstrapper::rootDirectory() . $uploadPath;
				exec("rsync -rtOXpal $fromPath $toPath");
			}

		} elseif (is_file($dir) && !is_dir(str_replace('.md', '', $dir)) && !is_dir(str_replace('.html', '', $dir))) {
			$extension = pathinfo($dir);
			$extension = $extension['extension'];

			$url = str_replace(Bootstrapper::rootDirectory() . self::$mdPathStatic, '', $dir);
			$url = str_replace(['.md','.html'], '', $url);
			$urlText = str_replace(['_', '-'], ' ', ucfirst(basename($dir, '.'.$extension)));
			$html .= "<li>
				<a $altColourClass href=\"$url\"><span>$urlText</span></a></li>";
			self::$counter++;
		}

		return $html;
	}

	/**
	 * Creates and returns a title based off of URL.
	 *
	 * @return string
	 */
	public static function getTitle (string $file = null): string {
		$ini = Bootstrapper::getIni();
		$title = '';
		if (isset($_SERVER['REQUEST_URI'])) {
			$defaultView = $ini['app']['default_view'] ?? 'index';
			$request = $_SERVER['REQUEST_URI'];

			$title .= $request;

			if ($request === '/' || $request === '/'.$defaultView) {
				return ucfirst($defaultView);
			}
		} elseif ($file !== null) {
			$title = $file;
			$title .= str_replace(['.md', '.html'], '', $title);
		}

		$title = ltrim(urldecode($title),'/');
		$title = preg_replace('/^.*\/\s*/', '', $title);
		$title = str_replace(['-','_'],' ',$title);

		return ucfirst($title);
	}

	/**
	 * Builds a single menu entry.
	 * @param string $dir
	 * @param string $altColourClass
	 * @return string
	 */
	private static function buildMenuEntry (string $dir, string $altColourClass): string {
		$html = '';
		// If $dir == 'documents', create a new unsorted list
		if (basename($dir) == 'documents') {
			$html .= '<ul>';
		} else {
			// If $dir is a markdown file
			if (is_file($dir.'.md')) {

				// Get the extension from the markdown file
				$extension = pathinfo($dir.'.md');
				$extension = $extension['extension'];

				// Remove all path information so only the file name, without the extension, is stored in $url
				$url = str_replace(Bootstrapper::rootDirectory() . self::$mdPathStatic, '', $dir);
				$url = str_replace('.md', '', $url);

				// Create the single list element
				$html .= "<li>
								<a $altColourClass href='$url'>
									<span>".str_replace('_',' ',
						ucfirst(basename($dir, '.'.$extension))).
					'</span></a><ul>';

			} else {
				// If $dir is not a markdown file, create a new list element containing an empty link, and a new unsorted list.
				$html .= "<li>
								<a $altColourClass href='#'>
									<span>".str_replace('_', ' ',
						ucfirst(basename($dir))).
					'</span></a><ul>';
			}
			self::$counter++;
		}

		return $html;
	}

	/**
	 * Loops through all the entries within a directory, and builds the elements for the menu
	 * @param string $dir
	 * @param string $altColourClass
	 * @param bool $alternatingColours
	 * @return string
	 */
	private static function loopThroughEntries (string $dir, string $altColourClass, bool $alternatingColours): string {
		$html = '';

		// If the $dir is not called "errors" or "uploads"
		if (basename($dir) != 'errors' && basename($dir) != 'uploads') {
			// build a menu entry
			$html .= self::buildMenuEntry($dir, $altColourClass);

			// Loop through the directory and create nested menus
			foreach (glob("$dir/*") as $path) {
				if (str_contains($path, '.md') || str_contains($path, '.html') || is_dir($path)) {
					$html .= self::createMenu($path, $alternatingColours, self::$counter);
				}
			}

			// If $dir == documents, end the unsorted list
			if (basename($dir) == 'documents') {
				$html .= '</ul>';
			}else {
				// If not, end the unsorted list and list element
				$html .= '</ul></li>';
			}
		}

		return $html;
	}

}