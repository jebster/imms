<?php

namespace Angus\Imms\php\classes;

class Bootstrapper {

	private static array|false $ini;

	/**
	 * Gets the configuration from the Ini file; returns an array of the entire configuration.
	 * @return array|false
	 */
	public static function getIni (): array|false {
		return self::$ini;
	}

	public static function setIni (string $iniPath = null): void {
		if ($iniPath === null) {
			self::$ini = parse_ini_file(Bootstrapper::rootDirectory() . '/config/config.ini', true, INI_SCANNER_TYPED);
		} else {
			self::$ini = parse_ini_file($iniPath, true, INI_SCANNER_TYPED);
		}

	}

	/**
	 * Static function to return the root directory of the project.
	 * @return string
	 */
	public static function rootDirectory (): string {
		// Change the second parameter to suit your needs
		return dirname(__FILE__, 4);
	}
}