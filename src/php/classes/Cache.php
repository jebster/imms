<?php

namespace Angus\Imms\php\classes;
require_once '../vendor/autoload.php';

use League\CommonMark\Environment\Environment;
use League\CommonMark\Exception\CommonMarkException;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension;
use League\CommonMark\Extension\TableOfContents\TableOfContentsExtension;
use League\CommonMark\Extension\Attributes\AttributesExtension;
use League\CommonMark\MarkdownConverter;
use Ueberdosis\CommonMark\HintExtension;
use League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;
use League\CommonMark\Extension\Footnote\FootnoteExtension;
use Zoon\CommonMark\Ext\YouTubeIframe\YouTubeIframeExtension;
use ElGigi\CommonMarkEmoji\EmojiExtension;
use N0sz\CommonMark\Marker\MarkerExtension;
use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\Exception\SassException;
use Sven\CommonMark\ImageMediaQueries\ImageMediaQueriesExtension;
use Jnjxp\CommonMarkWikiLinks\WikiLinkExtension;
use SimonVomEyser\CommonMarkExtension\LazyImageExtension;

class Cache {

	private string $htmlPath = 'assets/cache/';
	private string $checksumPath;
	private array|false $ini;
	private string $htmlExt = '.html';
	private Environment $environment;
	// Extension defaults are shown below
	private array $config;

	/**
	 * The Cache class takes care of rendering markdown into HTML.
	 * This is mostly used to pre-render the markdown and store it to a cache directory for faster load times.
	 * There are also methods for clearing caches.
	 */
	public function __construct () {
		$this->ini = Bootstrapper::getIni();
		$this->config = [
			'heading_permalink' => [
				'html_class' => 'heading-permalink',
				'id_prefix' => '',
				'fragment_prefix' => '',
				'insert' => 'after',
				'min_heading_level' => $this->ini['heading_permalinks']['min_heading_level'] ?? 1,
				'max_heading_level' => $this->ini['heading_permalinks']['max_heading_level'] ?? 6,
				'title' => $this->ini['heading_permalinks']['link_title'] ?? 'Link to header',
				'symbol' => $this->ini['heading_permalinks']['link_symbol'] ?? '🔗',
				'aria_hidden' => false
			],
			'external_link' => [
				'internal_hosts' => $this->ini['external_links']['internal_hosts'] ?? 'localhost',
				'open_in_new_window' => $this->ini['external_links']['open_in_new_window'] ?? true,
				'html_class' => 'external-link',
				'nofollow' => '',
				'noopener' => 'external',
				'noreferrer' => 'external',
			],
			'footnote' => [
				'backref_class' => 'footnote-backref',
				'backref_symbol' => $this->ini['footnotes']['backref_symbol'] ?? '↩',
				'container_add_hr' => $this->ini['footnotes']['add_hr'] ?? true,
				'container_class' => 'footnotes',
				'ref_class' => 'footnote-ref',
				'ref_id_prefix' => 'fnref:',
				'footnote_class' => 'footnote',
				'footnote_id_prefix' => 'fn:',
			],
			'youtube_iframe' => [
				'width' => (string)$this->ini['youtube_iframe']['width'] ?? 600,
				'height' => (string)$this->ini['youtube_iframe']['height'] ?? 300,
				'allow_full_screen' => $this->ini['youtube_iframe']['allow_fullscreen'] ?? true,
			],
			'table_of_contents' => [
				'html_class' => 'table-of-contents',
				'style' => $this->ini['toc']['style'],
				'min_heading_level' => $this->ini['toc']['min_heading_level'] ?? 1,
				'max_heading_level' => $this->ini['toc']['max_heading_level'] ?? 6,
				'normalize' => 'relative',
				'position' => 'placeholder',
				'placeholder' => $this->ini['toc']['placeholder'] ?? '[[_TOC_]]',
			],
			'allow_unsafe_links' => false
		];

		// Configure the Environment with all the CommonMark parsers/renderers
		$this->environment = new Environment($this->config);
		$this->environment->addExtension(new CommonMarkCoreExtension());
		$this->environment->addExtension(new HeadingPermalinkExtension());
		$this->environment->addExtension(new TableOfContentsExtension());
		$this->environment->addExtension(new GithubFlavoredMarkdownExtension());
		$this->environment->addExtension(new AttributesExtension());
		$this->environment->addExtension(new HintExtension());
		$this->environment->addExtension(new ExternalLinkExtension());
		$this->environment->addExtension(new FootnoteExtension());
		$this->environment->addExtension(new YouTubeIframeExtension());
		$this->environment->addExtension(new EmojiExtension());
		$this->environment->addExtension(new MarkerExtension());
		$this->environment->addExtension(new ImageMediaQueriesExtension());
		$this->environment->addExtension(new WikiLinkExtension());
		$this->environment->addExtension(new LazyImageExtension());

		$this->checksumPath = Bootstrapper::rootDirectory() . '/src/checksums/';
	}

	/**
	 * Renders a single markdown file into HTML.
	 * @param string $file
	 * @param array|null $settings
	 * @return void
	 * @throws CommonMarkException
	 * @throws SassException
	 */
	public function md2html (string $file, ?array $settings = null): void {

		$templatesDir = null;

		$rootDir = '';

		if ($settings !== null && $settings['cli'] === true) {
			$settings['no_scss'] = true;
			$settings['error_page'] = false;
			$templatesDir = $settings['templates_dir'];
			$cacheFolderStructure = $settings['output_dir'] ?? '';

		} else {
			// Create the folder structure for the cache and checksums (default)
			$cacheFolderStructure = $this->createCacheDirs($file, $settings);
			$checksumFolderStructure = $this->createChecksumDirs($file, $settings);
			$rootDir = Bootstrapper::rootDirectory();
		}

		// If the file exists
		if (file_exists($file)) {
			$converter = new MarkdownConverter($this->environment);

			// Compile SCSS
			if ($settings === null) {
				self::compileSCSS();
			} elseif ($settings['no_scss']) {
				echo "No SCSS used\n";
			}

			$basenameFile = basename($file);

			// Generate path to HTML file
			$htmlFile = str_replace('.md', $this->htmlExt, $basenameFile);
			$htmlFile = $cacheFolderStructure . $htmlFile;

			$fileContents = file_get_contents($file);

			if ($settings === null) {
				// Generate path to checksum file
				$checksumFile = $checksumFolderStructure . $basenameFile;

				// Get the contents of the file and create a checksum of it
				$checksum = hash_file('sha256', $file);

				$checksumPath = '/src/checksums/';

				$checksumFile = $rootDir . $checksumPath . $checksumFile;

				file_put_contents($checksumFile, $checksum);
			}

			// Get all templates
			$templates = $this->getTemplates($templatesDir);

			// Create HTML - make sure uploads are done properly
			$html = $templates['header'];
			$html .= $converter->convert($fileContents);
			$html .= $templates['footer'];
			$html = str_replace('uploads', '/uploads', $html);

			// Figure out the path to the HTML file
			if ($settings !== null && $settings['error_page'] === true) {
				$path = $this->ini['app']['error_html_path'] ?? '/web/assets/cache/errors/';
			} else {
				$path = $this->ini['app']['html_path'] ?? '/web/assets/cache/';
			}

			if (isset($settings) && $settings['cli'] === true) {
				$htmlFile = $rootDir . $htmlFile;
			} else {
				$htmlFile = $rootDir . $path . $htmlFile;
			}

			// Put $html into $htmlFile
			file_put_contents($htmlFile, $html);

		} else {
			echo "File '$file' doesn't exist.";
		}
	}

	/**
	 * Clears the HTML cache for a single URL path (usually just a single HTML file).
	 * Example:
	 * The url is example.com/php/functions/file_exists, the "urlPath" will then be "php/functions/file_exists".
	 * @param $urlPath
	 * @param bool $cli
	 * @return void
	 */
	public function clearCacheSingularURL ($urlPath, bool $cli = false): void {

		$path = $this->ini['app']['html_path'] ?? '/web/assets/cache/';

		if ($cli && file_exists(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt)) {
			unlink(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt);
		}

		if (file_exists(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt)) {
			unlink(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt);
		}
	}

	/**
	 * Clears the entire cache. Markdown will be rendered on first load.
	 * @param $cachePath
	 * @return void
	 */
	public function clearCache ($cachePath): void {
		$files = glob($cachePath . '*');
		foreach ($files as $file) {
			if (is_file($file)) {
				unlink($file);
			}
			if (is_dir($file)) {
				exec("rm -rf $file");
			}
		}
	}

	/**
	 * Compiles SCSS according to the configuration found in the config.ini file.
	 * @throws SassException
	 */
	public function compileSCSS (): void {
		// Compile SCSS
		$compiler = new Compiler();
		$scssPath = $this->ini['app']['scss_path'] ?? '/src/scss/';
		$cssPath = $this->ini['app']['css_path'] ?? '/web/assets/css/main.css';
		$file = $this->ini['app']['scss_file'] ?? 'main.scss';
		$compiler->SetImportPaths(Bootstrapper::rootDirectory() . $scssPath);
		$css = $compiler->compileString(
			file_get_contents(
				Bootstrapper::rootDirectory() .
				$scssPath .
				$file))
			->getCss();
		file_put_contents(Bootstrapper::rootDirectory() . $cssPath, $css);
	}

	/**
	 * Creates the necessary directories (also nested), to properly serve the cache.
	 * @param string $filepath
	 * @param array|null $settings
	 * @return string
	 */
	private function createCacheDirs (string $filepath, ?array $settings = null): string {
		// Fetch folder structure
		$folderStructure = $this->getFolderStructure($filepath, $settings);

		// Create the folders if they don't exist
		if (strlen($folderStructure) > 0) {
			if (!is_dir($this->checksumPath . $folderStructure)) {
				mkdir($this->checksumPath . $folderStructure, 0775, true);
			}
			$folderStructure .= '/';
		}

		return $folderStructure;
	}

	private function createChecksumDirs (string $filepath, ?array $settings = null): string {
		// Fetch folder structure
		$folderStructure = $this->getFolderStructure($filepath, $settings);

		// Create the folders if they don't exist
		if (strlen($folderStructure) > 0) {
			if (!is_dir($this->htmlPath . $folderStructure)) {
				mkdir($this->htmlPath . $folderStructure, 0775, true);
			}
			$folderStructure .= '/';
		}

		return $folderStructure;
	}

	/**
	 * Returns an array with the paths to the template files.
	 * Currently only looks for "header.html" and "footer.html"
	 * @param string|null $templatesDir
	 * @return array
	 */
	public function getTemplates (string $templatesDir = null): array {
		if ($templatesDir === null) {
			$templatesDir = Bootstrapper::rootDirectory() . '/src/templates/';
		}
		$templates = [];
		foreach (glob($templatesDir . '*.html') as $item) {
			if (str_contains($item, 'header.html')) {
				$item = file_get_contents($item);
				$metaDescription = $this->ini['app']['meta_description'] ?? 'IMMS is an almost-SSG, serving rendered markdown.';
				$mdPath = $this->ini['app']['md_path'] ?? '/src/documents/';
				$item = str_replace('[META_DESCRIPTION]', $metaDescription, $item);
				$item = str_replace('[TITLE]', Route::getTitle(), $item);
				$dir = Bootstrapper::rootDirectory() . $mdPath;
				$item = str_replace('[MENU]', Route::createMenu($dir, true), $item);
				$templates['header'] = $item;
			} elseif (str_contains($item, 'footer.html')) {
				$templates['footer'] = file_get_contents($item);
			}
		}

		return $templates;
	}

	/**
	 * Gets the folder structure of the file
	 * @param string $filepath
	 * @param array|null $settings
	 * @return string
	 */
	private function getFolderStructure(string $filepath, ?array $settings = null): string {
		if ($settings !== null && isset($settings['error_page']) && $settings['error_page'] === true) {
			$path = $this->ini['app']['error_md_path'] ?? '/src/errors/';
		} else {
			$path = $this->ini['app']['md_path'] ?? '/src/documents/';
		}
		$folderStructure = str_replace(Bootstrapper::rootDirectory() . $path, '', $filepath);
		return substr($folderStructure, 0, strrpos($folderStructure, '/'));
	}


}