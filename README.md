# IMMS - Inuitviking Markdown Management System

IMMS is a kind of SSG (Static Site Generator), but without you having to build it yourself and entirely without pipelines.

It takes your Markdown and renders it as HTML in a file cache, and serves this HTML without having to re-render it again.
This makes IMMS pretty fast at serving content.

## Intended use case

The intended use case is:

- You have a Git repository with Markdown (Perhaps a GitLab Wiki?)
- You pull from this repo regularly to IMMS
- You update this repo with new documentation or other markdown
- You let IMMS serve this content as web content

There's quite literally no fuss (or very little of it):

- No database
- No users that need to log in
- No need for manual cache handling
- No built-in editors

## Features

- GitHub Flavoured Markdown
- Header Permalinks
- Syntax highlighting for code blocks
- Hints
- Text Highlighting (Remember those yellow highlighters?)
- Youtube iframes
- Emojis
- Image attributes (size them however you want!)
- WikiLinks
- Sub pages
- Support for special characters in markdown file name
- GitLab style Table of Contents
- Automatic caching (and clearing of it!)
- A (albeit quite simple) CLI tool

## Additional features in Docker

- Makes use of PHP's opcache to improve performance
- Makes use of Apache's built-in caching, to serve files even faster

## Getting started

### Docker Compose

An example could be like so:
```yaml
services:
  imms:
    image: registry.gitlab.com/inuitviking/imms:latest
    ports:
      - 8080:80
    #    expose:
    #      - 1234
    environment:
      - APACHE_PORT=1234
      - APACHE_CACHE_AGE=900
      - APACHE_CACHE_SIZE=5M
    volumes:
      - ./.docker/documents:/var/www/src/documents
      - ./.docker/config:/var/www/config
```

- `./.docker/documents`: The place where you store our uploads and markdown files.
- `./.docker/config`: This is where the `config.ini` file is stored

All of the above is supplied in `.docker` as examples.

You may also see the Apache config and PHP config within `.docker`; this is used for building the docker image, to configure opcache and Apache's built in cache.

You can supply your own configuration, as with any docker image, with the use of volumes.

The docker image provided makes use of opcache and Apache's included caching system to improve response time.

### On a good ol' web server
```bash
git clone git@gitlab.com:InuitViking/imms.git
cd imms
composer install
mkdir -p src/documents/errors
touch src/documents/index.md
```

## Dependencies

- [league/commonmark](https://github.com/thephpleague/commonmark)
- [scssphp/scssphp](https://scssphp.github.io/scssphp/)
- [editor.md](https://github.com/pandao/editor.md) (not yet, but plan to)
- spatie/commonmark-highlighter
- ueberdosis/commonmark-hint-extension
- zoon/commonmark-ext-youtube-iframe
- elgigi/commonmark-emoji
- n0sz/commonmark-marker-extension

## Notes for dev

### How to release

Remember the version convention: year.month.day[.patch].

Create a tag:
```bash
git tag v[VERSION]
```

Go to [Releases](https://gitlab.com/InuitViking/imms/-/releases) and edit your release with the archives you've created with the following commands:
```bash
tar -zcvf imms-22.04.12.tar.gz --exclude="imms/.docksal" --exclude="imms/.git" --exclude="imms/.idea" --exclude="imms/.gitignore" --exclude="vendor" imms/
zip -r imms-22.04.12.zip imms -x "imms/.docksal/*" -x "imms/.git/*" -x "imms/.idea/*" -x imms/.gitignore -x "imms/vendor/*"
```