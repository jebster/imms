<?php

require_once '../vendor/autoload.php';

use Angus\Imms\php\classes\Route;
use Angus\Imms\php\classes\Bootstrapper;

Bootstrapper::setIni();
$route = new Route();